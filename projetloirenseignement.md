---
title: 'Du Projet de Loi Renseignement'
published: true
date: '31-03-2015 23:30'
taxonomy:
    category:
        - blog
        - 'Libres propos'
    tag:
        - loi
        - renseignement
        - philosophie
process:
    markdown: true
child_type: default
routable: true
cache_enable: true
visible: true
header_image: '1'
summary:
    enabled: '1'
    format: short
---

Le [projet de loi relatif au renseignement](http://www.legifrance.gouv.fr/Droit-francais/Actualite/Projet-de-loi-relatif-au-renseignement-PRMX1504410L-19-03-2015-pjl) qui sera examiné mi-avril en lecture d'urgence, ainsi que [le rapport](http://www.senat.fr/rap/r14-201/r14-201_mono.html) de la Délégation Parlementaire au Renseignement, par Jean-Jacques Urvoas, taxant au passage E. Snowden d'« idiot utile au service des groupe terroristes » (sic!), ne peuvent laisser personne de marbre. Le même Urvoas avait déjà présenté un rapport [plutôt critiqué](http://www.lepoint.fr/editos-du-point/jean-guisnel/renseignement-les-lacunes-du-rapport-urvoas-19-05-2013-1669756_53.php) sur le même thème avec un [projet de loi avorté](http://www.lepoint.fr/editos-du-point/jean-guisnel/renseignement-pavane-pour-une-loi-defunte-26-04-2012-1455289_53.php) en 2012\. Il repasse par la fenêtre. On s'étonnera cependant qu'aussi peu d'intelligences ne se dressent à l'encontre des idées véhiculées par ce nouveau projet de loi et les arguments qui le portent.

===

Si nous étions légions à critiquer les pratiques discutables des services de renseignement dénoncés à la suite de l'affaire Snowden, y compris sur le territoire français, les attentats à Charlie Hebdo au début de l'année laissaient présager qu'un Patriot Act à la française, depuis longtemps préparé (cf. [notre cadeau de Noël 2014](http://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000029958091&categorieLien=id)), trouvait là l'opportunité rêvée de passer comme une lettre à la Poste. À vrai dire, il ne fallait pas être grand devin pour deviner la suite des événements, même en les [dénonçant par avance](http://framablog.org/2015/01/12/patriot-act-francais-pour-nous-non/).

Les arguments de façade sont en gros les suivants : pour pallier au risque terroriste et d'espionnage qui mettent en danger l'État français, il ne faut pas tellement de nouvelles procédures judiciaires mais une légalisation de pratiques auparavant définies comme illégales. En d'autres termes, il faut autoriser des pratiques de surveillance auparavant définies comme contraires aux principes républicains et des droits de l'homme, et, pour les simplifier, ne pas s'encombrer des instruments de la Justice qui pourraient les invalider, en particulier les juges judiciaires, garants de l'égalité de traitement de chacun devant la loi. Mais le projet de loi récèle bien d'autres dangers, en particulier le fait que nombre de concepts restent volontairement flous (tels le « risque terroriste », pour n'en citer qu'un), et les moyens de surveillance généralisée (on dira : « étendue aux personnes susceptibles d'être des intermédiaires ») par des algorithmes d'analyse sur Internet, dont la principale fonction est de porter une atteinte évidente aux libertés individuelles. L'extensibilité d'une telle loi à d'autres secteurs que le seul « risque terroriste » est d'autant plus évident que les solutions de recours des individus sont pour ainsi dire inexistantes.

J'aime beaucoup les mots de l'écrivain Alain Damasio, auteur de la _Horde du Contrevent_ et de _La Zone du Dehors_ qui accole l'expression de [Big Mother](http://www.lesinrocks.com/2013/11/25/medias/lauteur-sf-alain-damasio-disseque-societe-controle-cest-pas-big-brother-cest-big-mother-11447605/) à notre société de contrôle, au lieu du traditionnel Big Brother d'Orwell. La différence entre les deux ? C'est que Big Mother, on l'aime, on l'embrasse, on la réclame. Alors que Big Brother est imposé au peuple par le Pouvoir Central. C'est sur cet effet Big Mother que comptent actuellement certains membres du Gouvernement français, pérorant _ad nauseam_ la sacro-sainte unité post-Charlie Hebdo. Oui, disent les sondages, les français sont prêts à abandonner leurs libertés individuelles pour davantage de sécurité. Dès lors l'État serait dans son droit le plus strict en rétablissant des principes que les grands Esprits européens, et surtout français, s'étaient échinés à découper, démonter et dénoncer pendant presque trois siècles.

J'estime pour ma part aujourd'hui, que l'ignorant des textes philosophiques a au moins l'excuse de ne pas forcément avoir eu le loisir des les côtoyer. On ne peut pas en dire autant de certains politiques dont l'ignorance crasse (souvent feinte à dessein) et la médiocrité n'ont d'égal que leur soif de pouvoir et de connivences. Mais le pire est sans doute chez ceux qui, au moins à peu près cultivés, connaissent parfaitement la provenance littéraire des grands principes de la Déclaration Universelle des Droits de l'Homme et (donc) du Citoyen que la France arbore si fièrement aux frontons de ses grands édifices. Car oui, en effet, le projet de loi relatif au renseignement renvoie directement à ces principes que j'estime bafoués.

Je me suis donc livré à ce petit exercice, et j'invite les lecteurs à faire de même sur leurs blogs et autres réseaux sociaux : pensez simplement à ce que vous avez déjà lu, il y a peut-être fort longtemps. Allez chercher les sources et citez-les... ou plutôt rappelez-les à vos amis, dans une version complète si possible. Ne le faites pas à la mode Twitter qui ne laisse que des bribes souvent mal comprises, voire de fausses citations ou des sources apocryphes. Car nous n'avons jamais autant rendu ces textes accessibles que sur Internet et c'est justement cet outil et l'extraordinaire accomplissement de la liberté de pensée, de connaissance et d'expression qu'il représente, qui est actuellement en grand danger. Et par extension, c'est notre démocratie qui l'est.

* * *

**Libre arbitre _vs._ l'État de droit. Du fait que nos politiques ne retiennent souvent de Spinoza que le premier paragraphe de cette citation tirée du Traité.**

[...] Or nous avons vu que la formation d’un État n’est possible qu’à cette condition, savoir : que le pouvoir de porter des décrets soit remis aux mains du peuple entier, ou de quelques hommes, ou d’un seul homme. Le libre jugement des hommes n’est-il pas infiniment varié ? Chacun ne croit-il pas savoir tout à lui seul ? N’est-il pas impossible que tous les hommes aient les mêmes sentiments sur les mêmes choses, et parlent d’une seule bouche ? Comment donc pourraient-ils vivre en paix si chacun ne faisait librement et volontairement l’abandon du droit qu’il a d’agir à son gré ? Chacun résigne donc librement et volontairement le droit d’agir, mais non le droit qu’il a de raisonner et de juger. Ainsi, quiconque veut respecter les droits du souverain ne doit jamais agir en opposition à ses décrets ; mais chacun peut penser, juger et par conséquent parler avec une liberté entière, pourvu qu’il se borne à parler et à enseigner en ne faisant appel qu’à la raison, et qu’il n’aille pas mettre en usage la ruse, la colère, la haine, ni s’efforcer d’introduire de son autorité privée quelque innovation dans l’État.

[...] Vouloir tout soumettre à l’action des lois, c’est irriter le vice plutôt que le corriger. Ce qu’on ne saurait empêcher, il faut le permettre, malgré les abus qui en sont souvent la suite. Que de maux ont leur origine dans le luxe, la jalousie, l’avarice, l’ivrognerie et autres mauvaises passions ! On les supporte, cependant, parce que les lois n’ont pas de moyen de les réprimer, bien que ce soient des vices réels ; à plus forte raison faut-il permettre la liberté de la pensée qui est une vertu et qu’on ne saurait étouffer. Ajoutez qu’elle ne donne lieu à aucun inconvénient que les magistrats, avec l’autorité dont ils sont revêtus, ne puissent facilement éviter, comme je le montrerai tout à l’heure. Je ne ferai pas même remarquer que cette liberté de la pensée est absolument nécessaire au développement des sciences et des arts, lesquels ne sont cultivés avec succès et bonheur que par les hommes qui jouissent de toute la liberté et de toute la plénitude de leur esprit.

[...] Or, puisqu’il est constant que la nature humaine est ainsi faite, ne s’ensuit-il pas que les lois qui concernent les opinions s’adressent, non pas à des coupables, mais à des hommes libres, qu’au lieu de réprimer et de punir des méchants, elles ne font qu’irriter d’honnêtes gens, qu’enfin on ne saurait, sans mettre l’État en danger de ruine, prendre leur défense ? Ajoutez à cela que des lois de cette nature sont parfaitement inutiles. En effet, considère-t-on comme saines et vraies les opinions condamnées par les lois, on n’obéira pas aux lois ; repousse-t-on au contraire comme fausses ces mêmes opinions, on acceptera alors les lois qui les condamnent comme une sorte de privilège, et on en triomphera à ce point que les magistrats, voulussent-ils ensuite les abroger, ne le pourraient pas.

Spinoza, _Traité théologico-politique_, [Chapitre XX](http://fr.wikisource.org/wiki/Trait%C3%A9_th%C3%A9ologico-politique/Chapitre_20), « On établit que dans un État libre, chacun a le droit de penser ce qu'il veut et de dire ce qu'il pense ».

* * *

**Du ridicule de la censure.**

« [...] on me dit que, pendant ma retraite économique, il s'est établi dans Madrid un système de liberté sur la vente des productions, qui s'étend même à celles de la presse ; et que, pourvu que je ne parle en mes écrits ni de l'autorité, ni du culte, ni de la politique, ni de la morale, ni des gens en place, ni des corps en crédit, ni de l'Opéra, ni des autres spectacles, ni de personne qui tienne à quelque chose, je puis tout imprimer librement, sous l'inspection de deux ou trois censeurs. Pour profiter de cette douce liberté, j'annonce un écrit périodique, et, croyant n'aller sur les brisées d'aucun autre, je le nomme _Journal inutile_. »

Beaumarchais, _Le Mariage de Figaro ou La Folle Journée_ ([acte V](http://fr.wikisource.org/wiki/Le_Mariage_de_Figaro/Acte_V), 3).

* * *

**Du fait que Montesquieu estime que seules les lois peuvent déterminer les limites de la liberté d'expression...**

Comme, pour jouir de la liberté, il faut que chacun puisse dire ce qu’il pense ; & que, pour la conserver, il faut encore que chacun puisse dire ce qu’il pense ; un citoyen, dans cet état, diroit & écriroit tout ce que les lois ne lui ont pas défendu expressément de dire, ou d’écrire.

Montesquieu, « Comment les lois peuvent contribuer et former les mœurs, les manières et le caractère d'une nation », _De l'Esprit des lois_ (Livre XIX, chap. XXVII).

* * *

**...Mais il faut une nécessaire séparation des pouvoirs (ne pas laisser une même classe faire les lois et les appliquer).**

Tout serait perdu, si le même homme, ou le même corps des principaux, ou des nobles, ou du peuple, exerçaient ces trois pouvoirs: celui de faire des lois, celui d'exécuter les résolutions publiques, et celui de juger les crimes ou les différends des particuliers.

Montesquieu, « De la constitution d'Angleterre », _De l'Esprit des lois_ (Livre XI, [chap. VI](http://mjp.univ-perp.fr/textes/montesquieu.htm)).

* * *

**La soif de pouvoir de ceux qui font les lois est aussi dangereuse pour l'État que les velléités d'un dictateur. Machiavel avait compris le sens et les leçons de l'Histoire...**

Le peuple et le sénat commirent des erreurs graves dans l’institution des [décemvirs](http://fr.wikipedia.org/wiki/D%C3%A9cemvir). Et quoique, dans le chapitre qui traite du dictateur, j’aie avancé que les seuls magistrats dangereux pour la liberté sont ceux qui s’emparent eux-mêmes du pouvoir, et non ceux que nomme le peuple, néanmoins ce dernier, quand il établit de nouveaux magistrats, doit les instituer de manière à ce qu’ils éprouvent quelque crainte à se laisser corrompre.

Une surveillance active aurait dû entourer sans cesse les décemvirs, et les maintenir dans le devoir ; les Romains ne les surveillèrent pas. Ils devinrent dans Rome l’unique tribunal ; tous les autres furent abolis. Et, comme nous l’avons déjà dit, c’est ainsi que l’extrême désir qu’avaient, et le sénat d’abolir les tribuns, et le peuple de détruire les consuls, aveugla tellement le peuple et le sénat, qu’ils ne balancèrent point à concourir tous deux au désordre général.

Aussi, le roi Ferdinand disait que les hommes imitent souvent ces faibles oiseaux de rapine, qui poursuivent avec un tel acharnement la proie que la nature leur indique, qu’ils ne s’aperçoivent pas d’un autre oiseau plus fort et plus puissant qui s’élance sur eux pour les déchirer.

On verra donc par ce que je viens de dire, ainsi que je me l’étais proposé en commençant ce chapitre, dans quelles fautes le désir de sauver la liberté précipita le peuple romain, et celles que commit Appius pour s’emparer de la tyrannie.

Machiavel, _Discours sur la première décade de Tite-Live_, Livre 1, [ch. XL](http://fr.wikisource.org/wiki/Discours_sur_la_premi%C3%A8re_d%C3%A9cade_de_Tite-Live/Livre_premier/Chapitre_40).